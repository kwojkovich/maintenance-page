job "maintenance-page" {
  datacenters = ["dc1"]

  group "maintenance-page" {
    network {
      port "http" {
        to = 80
      }
    }
    service {
      name = "maintenance-page"
      tags = ["urlprefix-/"]
      port = "http"

      check {
        type = "tcp"
	interval = "30s"
	timeout = "3s"
	port = "http"
      }
    }

    task "maintenance-page" {
      driver = "docker"

      config {
        image = "registry.gitlab.com/kwojkovich/maintenance-page"
        ports = ["http"]
      }

      resources {
        cpu    = 100
        memory = 128
      }
    }
  }
}
